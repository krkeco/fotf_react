import React, { Component } from 'react';
import logo from './Images/logo.svg';
import './App.css';
import Broadcasts from './Components/Broadcasts.js';
import { 
  Container, 
  Row, 
  Col, 
} from 'reactstrap';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
     
    };
  }

  render() {
    return (
      <div className="app">

        <header className="app-header">
        <Container>
          <Row>
            <Col  sm="12" lg={{size:10, offset:1}}>
              <img className="app-logo" width="60%" src={logo} />
            </Col>
          </Row> 
          <Col className="app-title"><strong>BROADCASTS</strong></Col>
        </Container>
          
        </header>

          <Broadcasts/>

      </div>
    );
  }
}

export default App;