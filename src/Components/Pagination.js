import React, { Component } from 'react';

import 'bootstrap/dist/css/bootstrap.css';
import { 
  Container, 
  Row, 
  Col,
  Button,
} from 'reactstrap';

//This component generates the pagination buttons.
//The if statement logic:
//The first half deals with pages 1-4,
//It shows all numbers from 1-9 if available.
//The second half deals with all pages beyond page 4.

// if this page is less than page 5
//   if this page is the current page
//     then make it the highlighted button
//   else if it is less than page 10
//     then make it a normal button
//   else
//     do nothing to it

// else if this page is within 4 of the current page 
//   if this page is the current page
//     then make it the highlighted button
//   else if page is exactly 4 away
//     then make the [...] button
//   else 
//     make a normal button
// else 
//   do nothing

export default class Pagination extends Component {
constructor(props) {
    super(props);
    this.state = {
    };
  }


  render(){

    return (
      <Container align="center">
      <Row>
      <Col xs="12" md="3" lg="3">
      <Button outline color="info"  onClick={() => {this.props.goToPage(1)}}>First</Button>
      <Button outline color="info"  onClick={() => {this.props.goToPage(this.props.pageNumber-1)}}>Prev.</Button>
      </Col>
      
      <Col xs="12" md="6" lg="6">
      {this.props.pages.map((page, i) => 
          this.props.pageNumber < 5 ? (
            page === this.props.pageNumber ? (

              <Button 
                size="sm"
                onClick={() => {this.props.goToPage(page)}}
              color="info" >
                {page}
              </Button>

            ) : (
              page < 10 ? (
                <Button
                  size="sm" 
                  onClick={() => {this.props.goToPage(page)}}
                  outline 
                  color="info" >
                    {page}
                </Button>
              ):(this.doNothing)
            )
          ) : (

          page >= this.props.pageNumber -4
          && page <= this.props.pageNumber +4 ? (

            page === this.props.pageNumber ? (

              <Button 
                size="sm"
                onClick={() => {this.props.goToPage(page)}}
                color="info" >
                {page}
              </Button>

            ) : (

              page === this.props.pageNumber +4 
                && this.props.pageNumber>4
              || page === this.props.pageNumber -4
                && this.props.pageNumber>4 ? (

                <Button outline size="sm">...</Button>
              ) : (


                <Button 
                  size="sm"
                  onClick={() => {this.props.goToPage(page)}}
                  outline 
                  color="info" >
                    {page}
                </Button>
              )
            )

          ):(this.doNothing)
            
          )
      )}
      </Col>
      
      <Col xs="12" md="3" lg="3">
      <Button outline color="info" onClick={() => {this.props.goToPage(this.props.pageNumber+1)}}>Next</Button>
      <Button outline color="info" onClick={() => {this.props.goToPage(this.props.pageCount)}}>Last</Button>
      </Col>
      </Row>
    </Container>
    );
  }
}