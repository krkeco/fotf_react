import React, { Component } from 'react';

import 'bootstrap/dist/css/bootstrap.css';
import { 
  Container, 
  Row, 
  Col,
} from 'reactstrap';

//This is the archived content component.
export default class ArchiveCard extends Component {
constructor(props) {
    super(props);
    this.state = {
      loaded: false,
    };
    this.getDateString = this.getDateString.bind(this);
    this.isLoaded = this.isLoaded.bind(this);
  }

getDateString(){
  //Initial time is ISO format, so we conver to millis, then string.
  let d = Date.parse(this.props.broadcast.broadcast_date);
  let string = new Date(d).toLocaleDateString();
  return string;
}
isLoaded(){
  this.setState({loaded: true});
}

  render(){
    let loader = 
      <div className="loader"/>;
    if(this.state.loaded){
      loader = null;
    }

    return (

      <Col xs="12" sm="12" md="6" >
        <Container>
          <Row>
            <card className="archive-broadcast"
              onClick={() => {
              this.props.onClick(this.props.archiveIndex);
              this.props.shade;
              }}>
              <Container>
                <Row  xs="5" sm="5" md="8">
                  <Col align="left" xs="8" sm="8" md="8">{this.props.broadcast.title}</Col>
                  <Col align="right" xs="4" sm="4" md="4">{this.getDateString(this.props.broadcast.broadcast_date)}</Col>
                </Row>

                {loader}

                <img 
                  onLoad={this.isLoaded}
                  width='80%' 
                  className="rounded-border" 
                  src={this.props.broadcast.primary_image} />
              

                <Row  xs="5" sm="5" md="8">
                  <Col align="left" xs="12" sm="12" md="12">
                  {'Featuring: '+this.props.broadcast.guests.map((guest, j) => 
                      " " + guest.first_name
                  )}
                  </Col>
                </Row>

              </Container>
            </card>
          </Row>
        </Container>
      </Col>

    );
  }
}