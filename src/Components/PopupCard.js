import React, { Component } from 'react';

import 'bootstrap/dist/css/bootstrap.css';
import { 
  Container, 
  Row, 
  Col,
} from 'reactstrap';

//This is the broadcast details component.
export default class PopupCard extends Component {
constructor(props) {
    super(props);
    this.state = {
      loaded: false,
    };
    this.getDateString = this.getDateString.bind(this);
    this.isLoaded = this.isLoaded.bind(this);
  }

getDateString(date,i){
  //Initial time is ISO format, so we conver to millis, then string
  let d = Date.parse(this.props.broadcast.broadcast_date);
  let string = new Date(d).toLocaleDateString();
  return string;
}
isLoaded(){
  //Turns the image loading animation off
  this.setState({loaded: true});
}

  render(){

    //This checks if image is loaded, and then sets state.
    let loader = 
      <div className="loader"/>;
    if(this.state.loaded){
      loader = null;
    }

    return (
    <div>
      <Row>
        <Container>
          {this.getDateString(this.props.broadcast.broadcast_date,this.props.currentBroadcast)}
        </Container>

        <Col xs="12" sm="6" md="6" align="center">
          <img
          width='100%' 
          className="rounded-border" 
          src={this.props.broadcast.primary_image}/>
        
          <audio controls className="audio-player">
            <source src="https://www.w3schools.com/html/horse.ogg" type="audio/ogg"/>
            <source src="https://www.w3schools.com/html/horse.mp3" type="audio/mpeg"/>
            Your browser does not support the audio element.
            <a href="https://www.w3schools.com/html/horse.mp3">Download</a>
          </audio>
        </Col>

        <Col>
          <div align="left">
          <strong>Hosted by:</strong>
          </div>
          <Row className="container-fluid">
          {this.props.broadcast.hosts.map((host, j) =>
            
            <Col xs="4">
              {loader}
              <img 
                align="center"
                onLoad={this.isLoaded}
                width='75%' 
                className="rounded-border" 
                src={this.props.broadcast.hosts[j].primary_image}/>
              <div align="center" className="hosts-font">{this.props.broadcast.hosts[j].first_name}</div>
            </Col> )}
          </Row>

        </Col>
      </Row>

      <Row  xs="5" sm="5" md="8">
        <Col align="left" xs="12" sm="12" md="12">
        {'Featuring: '+this.props.broadcast.guests.map((guest, j) => 
            " " + guest.first_name
        )}
        </Col>
      </Row>


      <div align="left">
        <strong>Description: </strong>{this.props.broadcast.description}
      </div>           
      <div align="left">
        <strong>Related Resources: </strong>
        {this.props.broadcast.resources.map((resource, j) => 
          " " + resource.url +","
        )}
      </div>

    </div>
    );
  }
}