import React, { Component } from 'react';

import 'bootstrap/dist/css/bootstrap.css';
import { 
  Container, 
  Row, 
  Col,
} from 'reactstrap';

//This is the broadcast of the day component.
export default class MainCard extends Component {
constructor(props) {
    super(props);
    this.state = {
      loaded: false,
    };
    this.getDateString = this.getDateString.bind(this);
    this.isLoaded = this.isLoaded.bind(this);
  }

getDateString(){
  //Initial time is ISO format, so we conver to millis, then string.
  let d = Date.parse(this.props.broadcast.broadcast_date);
  let string = new Date(d).toLocaleDateString();
  return string;
}
isLoaded(){
  this.setState({loaded: true});
}

  render(){
    let loader = 
      <div className="loader"/>;
    if(this.state.loaded){
      loader = null;
    }

    return (
      <Container >
        <Row >
          <card  className="bg-gray"
            onClick={() => {
              this.props.onClick(0);
            }}>
            <Container >
              <Row >
                <Col  xs="12" sm="12" md="6">
                  <div align="left">
                    <strong>{this.props.broadcast.title}</strong>
                  </div>
                    {loader}
                    <img 
                      onLoad={this.isLoaded}
                      width='100%' 
                      className="rounded-border" 
                      src={this.props.broadcast.primary_image}/>
                </Col>
              
                <Col  xs="12" sm="12" md="6">
                  <div align="left">
                    <div>Broadcast for today: {this.getDateString(this.props.broadcast.broadcast_date)}</div>
                   
                   
                    <Row  xs="5" sm="5" md="8">
                      <Col align="left" xs="12" sm="12" md="12">
                      {'Featuring: '+this.props.broadcast.guests.map((guest, j) => 
                          " " + guest.first_name
                      )}
                      </Col>
                    </Row>

                            
                    <strong>Hosted by:</strong>

                    <Row>
                      {this.props.broadcast.hosts.map((host, j) =>
                        <Col xs="4">
                          <img 
                            width='80%' 
                            className="rounded-border" 
                            src={this.props.broadcast.hosts[j].primary_image} />
                          <div align="center" className="hosts-font">{this.props.broadcast.hosts[j].first_name}</div>
                        </Col> 
                      )}
                    </Row>
                  </div>
                </Col>
              </Row>
            </Container>
          </card>
        </Row>
      </Container>
    );
  }
}