import React, { Component } from 'react';

import 'bootstrap/dist/css/bootstrap.css';
import { 
  Container, 
  Row, 
  Col, 
  Card,
  Button,
} from 'reactstrap';

//A library that makes a popup card. 
import Skylight from 'react-skylight';

//Fills the skylight with content.
import PopupContentCard from './PopupCard.js';

//Content for broadcast of the day.
import MainCard from './MainCard.js';

//Content for broadcasts that are archived.
import ArchiveCard from './ArchiveCard.js';

//Pagination interface.
import Pagination from './Pagination.js';

export default class Broadcasts extends Component {
constructor(props) {
    super(props);
    this.state = {
      //Paginate variables.
      pageNumber: 1,
      pageCount: 1,
      pages: [1],
      
      //This stores the entire JSON file from server.
      broadcasts:[
        {title: '',
        guests:[{first_name:''}],
        hosts:[{first_name:'',
        primary_image:''}],
        resources:[{url:''}],
        description:''},
      ],

      //This is for PopupCard.
      currentBroadcast: 0,

    };
    this.goToPage = this.goToPage.bind(this);

    this.getDateString = this.getDateString.bind(this);

    this.popupCard = this.popupCard.bind(this);
  }

componentDidMount(){
  //GET broadcast data from server.
  //If you setup a localhost change the fetch code.
  //You also want to change the port to -p 3001 so it doesn't step on this server.
  fetch('https://warm-meadow-82043.herokuapp.com/broadcasts.json')
  //fetch('http://localhost:3001/broadcasts.json')
  .then(response => response.json())
  .then(data => {

    this.setState({broadcasts: data.broadcasts})

      let pageCount = Math.ceil(data.broadcasts.length/10);
      this.setState({pageCount: pageCount});
      var pageViews = [];
      for(let x = 1; x <= pageCount; x++){
        pageViews.push(x);
      }
      this.setState({pages: pageViews});
    });

  }

getDateString(date,i){
  //Initial time is in ISO format-- We convert it to millis, then string.
  let d = Date.parse(this.state.broadcasts[i].broadcast_date);
  let string = new Date(d).toLocaleDateString();
  return string;
}

goToPage(x){
   if(x>0
   && x<=this.state.pageCount){
    this.setState({pageNumber: x});
  }
}

//This is a callback for onClick from the broadcast cards.
//It will open a PopupCard with the details/audio of the broadcast.
popupCard(i){
  this.setState({currentBroadcast: i});
  this.simpleDialog.show();
}
  render(){
    //Styling for the Skylight container.
    var skylight = {
      backgroundColor: 'skyblue',
      color: '#ffffff',
      width: '90%',
      height: '85%',
      marginTop: '-20em',
      marginLeft: '-45%',
    };

    return (
    <div className='padding-top'>

      <Container>
        <Skylight hideOnOverlayClicked dialogStyles={skylight}  
          ref={ref => this.simpleDialog = ref} 
          title={this.state.broadcasts[this.state.currentBroadcast].title}>

          <PopupContentCard broadcast={this.state.broadcasts[this.state.currentBroadcast]} currentBroadcast={this.state.currentBroadcast}/>
        
        </Skylight>
      </Container>

      <MainCard broadcast={this.state.broadcasts[0]} onClick={this.popupCard} currentBroadcast={this.state.currentBroadcast}/>
       

      <Pagination
        pageNumber={this.state.pageNumber}
        pageCount={this.state.pageCount}
        pages={this.state.pages}
        goToPage={this.goToPage}/>

      <Container className="padding-bottom padding-top">

        <Row>
          {this.state.broadcasts.map((broadcast, i) => 
            i < this.state.pageNumber*10 
            && i > (this.state.pageNumber-1)*10 +1 ? (
            
              <ArchiveCard 
                key={i}
               broadcast={this.state.broadcasts[i]}
                onClick={this.popupCard} 
                currentBroadcast={this.state.currentBroadcast}
                archiveIndex={i}/>
      
            ):(
              <div key={i}/>
            )
          )}
        </Row>
      </Container>

        <Col>
          <Pagination
            pageNumber={this.state.pageNumber}
            pageCount={this.state.pageCount}
            pages={this.state.pages}
            goToPage={this.goToPage}/>
        </Col>

    </div>
    );
  }
}